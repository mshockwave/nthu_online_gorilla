package nthu_ogs;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.*;

public class IOThread extends Thread{
    private NativeProvider mNative;
    private Socket mIOInstance;
    private static long TIMEOUT_MILLIS;
    private Timer mConnectionTimer = new Timer();

    private int mTimeLimit;
    private int mLevelTotal;
    private int mCurrentLevel = 1;

    private int mScoreTotal = 0;

    public IOThread(NativeProvider nativeProvider, Socket ioSocket, long timeoutMillis){
        this.mNative = nativeProvider;
        this.mIOInstance = ioSocket;
        TIMEOUT_MILLIS = timeoutMillis;

        initIO();
    }

    private void initIO(){
        mIOInstance.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                mConnectionTimer.cancel(); //Remove connection timeout timer
                mConnectionTimer.purge();

                System.err.println("Connect to server");
                System.err.flush();

                mNative.showInfoNative("Connect succeed!!");
            }
        }).on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                mNative.cleanNative();
                System.err.close();
             }
        }).on("userName", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                String userName = mNative.userNameWindowNative();
                while (userName == null || userName.length() <= 0) {
                    mNative.showInfoErrorNative("Name empty");
                    userName = mNative.userNameWindowNative();
                }

                try {
                    mIOInstance.emit("userName", (new JSONObject()).put("name", userName));
                } catch (JSONException e){
                    e.printStackTrace(System.err);
                }

            }
        }).on("playerList", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                JSONObject data = (JSONObject)args[0];
                System.err.println("Got playerList");

                try {
                    int requireNum = data.getInt("requireNum");
                    mTimeLimit = data.getInt("timeLimit");
                    mLevelTotal = data.getInt("levelNum");
                    JSONArray playerNames = data.getJSONArray("players");
                    String[] names = new String[playerNames.length()];

                    int i;
                    for(i = 0; i < playerNames.length(); i++){
                        names[i] = playerNames.getString(i);
                    }
                    mNative.playerListWindowNative(requireNum, (float) mTimeLimit / 1000f, names);
                } catch (JSONException e) {
                    e.printStackTrace(System.err);
                    System.err.flush();
                }
            }
        }).on("gameStart", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                mNative.showGameStartNative();
                try {
                    mIOInstance.emit("rect", (new JSONObject()).put("level", mCurrentLevel));
                } catch (JSONException e) {
                    e.printStackTrace(System.err);
                }
            }
        }).on("rect", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                JSONObject data = (JSONObject)args[0];
                try {
                    if(mCurrentLevel == data.getInt("level")){
                        mNative.clearRectsNative();
                        JSONArray rects = data.getJSONArray("rects");
                        int rectSize = data.getInt("size");

                        int i, x, y;
                        JSONObject element;
                        List<Integer> indexList = new ArrayList<Integer>();
                        for(i = 0; i < rects.length(); i++) indexList.add(i);
                        Collections.shuffle(indexList);

                        for(Integer index : indexList){
                            element = rects.getJSONObject(index);
                            x = element.getInt("x");
                            y = element.getInt("y");

                            mNative.addRectNative(x, y, rectSize, index + 1);
                        }

                        startNewLevel();
                    }
                } catch (JSONException e) {
                    e.printStackTrace(System.err);
                }
            }
        });
    }

    private void startNewLevel(){
        try {
            mNative.showNumbersNative();
            Thread.sleep(3500);
            long startTime = System.currentTimeMillis();
            int result = mNative.playWindowNative();
            if(System.currentTimeMillis() - startTime > (long)mTimeLimit){
                //TLE
                result = -1;
            }

            if(result < 0){
                mNative.showResultNative(2);
            }else if(result < 9){
                mNative.showResultNative(1);
            }else{
                mNative.showResultNative(0);
            }
            mScoreTotal += result;
            mIOInstance.emit("result", (new JSONObject()).put("count", result).put("level", mCurrentLevel));
            mNative.setCoverVisibleNative();
            //mNative.clearRectsNative();

            if(mCurrentLevel + 1 <= mLevelTotal){
                mCurrentLevel++;
                mIOInstance.emit("rect", (new JSONObject()).put("level", mCurrentLevel));
            }else{
                mNative.scoreWindowNative(mScoreTotal);
                mIOInstance.close();
            }
        } catch (InterruptedException e) {
            e.printStackTrace(System.err);
        } catch (JSONException e) {
            e.printStackTrace(System.err);
        }
    }

    @Override
    public void run(){
        mIOInstance.connect();
        mConnectionTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                /*TODO: Show connection timeout message*/
                mIOInstance.close();
                mNative.cleanNative();
                System.err.close();
            }
        }, TIMEOUT_MILLIS);
    }
}
