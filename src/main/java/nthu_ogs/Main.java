package nthu_ogs;

import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.net.URISyntaxException;

public class Main {

    private static NativeProvider sNativeProvider = new NativeProvider();
    private static final String LOG_FILE = "/Volumes/Main_Log/i2p.log";

    public static void main(String[] args){

        //Setup the log file
        try{
            PrintStream logStream = new PrintStream(new BufferedOutputStream(new FileOutputStream(LOG_FILE, true)));
            System.setErr(logStream);
        }catch(FileNotFoundException e){
            System.err.println("Can not open log file " + LOG_FILE);
        }

        sNativeProvider.initNative();

        sNativeProvider.showWelcomeNative();
        String address = sNativeProvider.connectWindowNative();
        try {
            Socket socket = IO.socket( (address.startsWith("http"))? address : ("http://" + address) );
            sNativeProvider.showInfoNative("Connecting...");
            IOThread ioThread = new IOThread(sNativeProvider, socket, 10 * 1000);
            ioThread.start();
        } catch (URISyntaxException e) {
            e.printStackTrace(System.err);
            sNativeProvider.cleanNative();
            System.err.close();
        }

    }
}
