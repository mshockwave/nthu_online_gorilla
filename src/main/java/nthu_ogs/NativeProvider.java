package nthu_ogs;

public class NativeProvider {
    native public void initNative();
    native public void cleanNative();
    native public void waitForKey();

    native public void showWelcomeNative();
    native public void setCoverVisibleNative();
    native public void showNumbersNative();
    native public int playWindowNative();
    native public void addRectNative(int col, int row, int size, int val);
    native public void clearRectsNative();
    native public String connectWindowNative();
    native public String userNameWindowNative();
    native public void playerListWindowNative(int requireNum, float timeLimit, String[] players);
    native public void scoreWindowNative(int score);

    native public void showInfoErrorNative(String msg);
    native public void showInfoNative(String msg);
    native public void showGameStartNative();
    native public void showResultNative(int resultCode);

    static{
        System.loadLibrary("gameNative");
    }
}
