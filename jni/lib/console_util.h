#ifndef _CONSOLE_UTIL_H_
#define _CONSOLE_UTIL_H_

#define gFrameRate	100/*HZ*/

#define DEFAULT_HEIGHT	40
#define DEFAULT_WIDTH	160

#define TO_STR(s)   #s

void printMiddle(const char*, int);
void printMiddleHor(const char*, int, int);
//void printMiddleVer(const char*, int, int);

void delay(void);
void pause(long);

void initColorStorage(void);
int getColor(int, int);
void destroyColorStorage(void);

#endif