#ifndef _GAME_UTIL_H
#define _GAME_UTIL_H

#include <ncurses.h>

typedef enum role{
	SERVER,
	CLIENT,
} Role;
typedef enum level {
	EASY,
	NORMAL,
	HARD,
} Level;

typedef struct config {
	Role role;
	Level level;
	int roundNum;
} Configs;

typedef struct gameRect {
	int val;
	int color;
	bool coverVisible;

	int size;
	int row, col;
} GameRect;
#define RECT_POOL_SIZE	20

//#define ROUND_MAX	20

#define ARRAY_SIZE(a)	( (int)(sizeof(a) / sizeof( (a)[0] )) )

#define CH_SIZE	7
#define CH_PADDING	2

void initRectsPool(void);
void addRect(GameRect);
GameRect* getRect(int);
int getRectsNum(void);
void clearRects(void);
void destroyRects(void);

void renderCover(bool, int, int);
void renderVal(bool);
void setCoversVisibled(void);

int renderString(const char*, int, int, int);

#define ICON_SIZE_MAX	31
void initIcons(void);
int renderIcon(char[ICON_SIZE_MAX][ICON_SIZE_MAX], int, int, int);
int getIconWidth(char[ICON_SIZE_MAX][ICON_SIZE_MAX]);
int getIconHeight(char[ICON_SIZE_MAX][ICON_SIZE_MAX]);

#endif
