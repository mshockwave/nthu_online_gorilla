#include <stdlib.h>
#include <string.h>
#include <ncurses.h>

#include "ansi_lexer.h"

extern FILE *yyin;
extern int yylex(void);

int currentRow = 0;
int currentCol = 0;
char buffer[300];

void pushChar(char ch){
	int index = strlen(buffer);
	buffer[index] = ch;
}

void flushAndPrint(){
	mvprintw(currentRow, currentCol, "%s", buffer);
	getyx(stdscr, currentRow, currentCol);
	memset(buffer, '\0', 300);
}

int main(){

	/*Init curses*/
	initscr();
	cbreak();
	noecho();
	start_color();

	/*Init string buffer*/
	memset(buffer, '\0', 300);

	yyin = fopen("test_ansi.txt", "r");
	if(yyin == NULL) exit(1);

	do{
		yylex();
	}while(!feof(yyin));
	refresh();

	getch();
	fclose(yyin);
	endwin();

	return 0;
}