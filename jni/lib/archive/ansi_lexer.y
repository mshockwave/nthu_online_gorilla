%option noyywrap
%{
#include <ncurses.h>
#include "ansi_lexer.h"

bool isParsing = FALSE;
int currentBg = COLOR_BLACK;
int currentFg = COLOR_WHITE;

void set_pair(int fg, int bg, bool isBold);
%}

%%
"*[m"	{
	flushAndPrint();

	attrset(A_NORMAL);
	isParsing = FALSE;
	attron(A_DIM); //Default is dim color
}

"*["	{
	flushAndPrint();

	isParsing = TRUE;
	attroff(A_BOLD);
}

"m"	{
	if(isParsing == TRUE){
		isParsing = FALSE;
	}else{
		pushChar('m');
	}
}

";"	{
	if(isParsing == TRUE){
		/*ignore*/
	}else{
		pushChar(';');
	}
}

"1"	{ //Bright color
	if(isParsing == TRUE){
		attroff(A_DIM);
	}else{
		pushChar('1');
	}
}

"4"	{ //Under line
	if(isParsing == TRUE){
		attron(A_UNDERLINE);
	}else{
		pushChar('4');
	}
}

"7"	{
	if(isParsing == TRUE){
		attron(A_REVERSE);
	}else{
		pushChar('7');
	}
}

[3-4][0-7]	{
	int num;
	sscanf(yytext, "%d", &num);

	if(num >= 30 && num < 40){ //Foreground
		switch(num){
		case 30:
		set_pair(COLOR_BLACK, currentBg, TRUE);
		break;

		case 31:
		set_pair(COLOR_RED, currentBg, FALSE);
		break;

		case 32:
		set_pair(COLOR_GREEN, currentBg, FALSE);
		break;

		case 33:
		set_pair(COLOR_YELLOW, currentBg, FALSE);
		break;

		case 34:
		set_pair(COLOR_BLUE, currentBg, FALSE);
		break;

		case 35:
		set_pair(COLOR_MAGENTA, currentBg, FALSE);
		break;

		case 36:
		set_pair(COLOR_CYAN, currentBg, FALSE);
		break;

		case 37:
		set_pair(COLOR_WHITE, currentBg, FALSE);
		break;
		}
	}else if(num >= 40 && num <= 47){ //Background
		switch(num){
		case 40:
		set_pair(currentFg, COLOR_BLACK, FALSE);
		break;

		case 41:
		set_pair(currentFg, COLOR_RED, FALSE);
		break;

		case 42:
		set_pair(currentFg, COLOR_GREEN, FALSE);
		break;

		case 43:
		set_pair(currentFg, COLOR_YELLOW, FALSE);
		break;

		case 44:
		set_pair(currentFg, COLOR_BLUE, FALSE);
		break;

		case 45:
		set_pair(currentFg, COLOR_MAGENTA, FALSE);
		break;

		case 46:
		set_pair(currentFg, COLOR_CYAN, FALSE);
		break;

		case 47:
		set_pair(currentFg, COLOR_WHITE, TRUE);
		break;
		}
	}

	attron(COLOR_PAIR(1));
}

.	{
	pushChar(yytext[0]);
}

%%

void set_pair(int fg, int bg, bool isBold){
	init_pair(1, fg, bg);
	currentFg = fg;
	currentBg = bg;

	if(isBold == TRUE) attron(A_BOLD);
}