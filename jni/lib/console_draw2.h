#ifndef _CONSOLE_DRAW_H_
#define _CONSOLE_DRAW_H_

extern void putASCII2( int x, int y, int ch, int fgcolor, int bgcolor );
extern void drawCmdWindow();
extern void clearScreen();

#endif