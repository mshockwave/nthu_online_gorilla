#include <ncurses.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include <game_util.h>
#include <console_util.h>
#include <console_draw2.h>
#include <kb_input2.h>

extern Configs gConfigs;

/*Icon utilities*/
char gIconGorillaNormal[ICON_SIZE_MAX][ICON_SIZE_MAX];
char gIconGorillaHappy[ICON_SIZE_MAX][ICON_SIZE_MAX];
char gIconGorillaMood[ICON_SIZE_MAX][ICON_SIZE_MAX];

#define INIT_ICON_META(FILE_NAME, NAME)	\
for(i = 0; i < ICON_SIZE_MAX; i++) memset(gIcon##NAME, '\0', sizeof(char) * ICON_SIZE_MAX);	\
fd = fopen(TO_STR(FILE_NAME), "r");	\
lineCounter = 0;	\
do{	\
	if( fgets(lineBuffer, ICON_SIZE_MAX, fd) != NULL ){	\
		strncpy(gIcon##NAME[lineCounter], lineBuffer, ICON_SIZE_MAX - 1);	\
		lineCounter++;	\
	}	\
}while(!feof(fd) && lineCounter < ICON_SIZE_MAX);	\
fclose(fd);


void initIcons(void){
	char lineBuffer[ICON_SIZE_MAX];
	int lineCounter = 0;
	int i;
	FILE *fd;

#ifndef ASCII_IMG_DIR
#define ASCII_IMG_DIR	"."
#endif

	//Icon normal
	INIT_ICON_META( ASCII_IMG_DIR/ascii_gorilla_normal.txt, GorillaNormal );
	//Icon happy
	INIT_ICON_META( ASCII_IMG_DIR/ascii_gorilla_happy.txt, GorillaHappy );
	//Icon mood
    INIT_ICON_META( ASCII_IMG_DIR/ascii_gorilla_mood.txt, GorillaMood );
}
int renderIcon(char icon[ICON_SIZE_MAX][ICON_SIZE_MAX], int startX, int startY, int color){
	int lineTotal = ICON_SIZE_MAX, i, j;
	int maxX = 0;

	for(i = 0; i < lineTotal; i++){
		if(strlen(icon[i]) <= 0) break;

		for(j = 0; j < strlen(icon[i]); j++){
			putASCII2(startX + j, startY + i, icon[i][j], color, COLOR_BLACK);
			if(startX + j + 1 > maxX) maxX = (startX + j + 1);
		}
	}

	return maxX;
}
int getIconWidth(char icon[ICON_SIZE_MAX][ICON_SIZE_MAX]){
	int lineTotal = ICON_SIZE_MAX, i;
    int maxX = 0, lineLength;

	for(i = 0; i < lineTotal; i++){
		if( (lineLength = strlen(icon[i])) <= 0 ){
			break;
		}else{
			if(lineLength > maxX) maxX = lineLength;
		}
	}

	return maxX;
}
int getIconHeight(char icon[ICON_SIZE_MAX][ICON_SIZE_MAX]){
	int lineTotal = ICON_SIZE_MAX, i;

	for(i = 0; i < lineTotal; i++){
		if(strlen(icon[i]) <= 0) return (i + 1);
	}

	return ICON_SIZE_MAX;
}

/*Game rectangle utilities*/
GameRect *gameRects[RECT_POOL_SIZE];
int rectPoolSize = 0;
//Rectangles storing
void initRectsPool(){
	int i;
	for(i = 0; i < RECT_POOL_SIZE; i++){
		gameRects[i] = NULL;
	}
	rectPoolSize = 0;
}
void addRect(GameRect rect){
	if(gameRects[rectPoolSize] == NULL){
		gameRects[rectPoolSize] = (GameRect*)malloc(sizeof(GameRect));
	}

	GameRect *currentRect = gameRects[rectPoolSize];
	currentRect -> val = rect.val;
	currentRect -> color = rect.color;
	currentRect -> coverVisible = rect.coverVisible;
	currentRect -> size = rect.size;
	currentRect -> row = rect.row;
	currentRect -> col = rect.col;

	rectPoolSize++;
}
void clearRects(){
	rectPoolSize = 0;
}
void destroyRects(){
	int i;
	for(i = 0; i < RECT_POOL_SIZE; i++){
		if(gameRects[i] != NULL) free(gameRects[i]);
	}
}
int getRectsNum(){
	return rectPoolSize;
}
GameRect* getRect(int index){
	return gameRects[index];
}

//Rectangles rendering
#define chEleRight	{'#', '#', '#', '#', '#', '#', '*'}
#define chEleLeft	{'*', '#', '#', '#', '#', '#', '#'}
#define chEleSides	{'*', '#', '#', '#', '#', '#', '*'}
#define chEleMiddle	{'#', '#', '#', '*', '#', '#', '#'}
#define chEleBar	{'*', '*', '*', '*', '*', '*', '*'}

const char chOne[CH_SIZE][CH_SIZE] = {
	{'#', '#', '*', '*', '#', '#', '#'},
	chEleMiddle,
	chEleMiddle,
	chEleMiddle,
	chEleMiddle,
	chEleMiddle,
	{'#', '#', '*', '*', '*', '#', '#'}
};
const char chTwo[CH_SIZE][CH_SIZE] = {
	chEleBar,
	chEleRight,
	chEleRight,
	chEleBar,
	chEleLeft,
	chEleLeft,
	chEleBar
};
const char chThree[CH_SIZE][CH_SIZE] = {
	chEleBar,
	chEleRight,
	chEleRight,
	chEleBar,
	chEleRight,
	chEleRight,
	chEleBar
};
const char chFour[CH_SIZE][CH_SIZE] = {
	chEleSides,
	chEleSides,
	chEleSides,
	chEleBar,
	chEleRight,
	chEleRight,
	chEleRight
};
const char chFive[CH_SIZE][CH_SIZE] = {
	chEleBar,
	chEleLeft,
	chEleLeft,
	chEleBar,
	chEleRight,
	chEleRight,
	chEleBar
};
const char chSix[CH_SIZE][CH_SIZE] = {
	chEleLeft,
	chEleLeft,
	chEleLeft,
	chEleBar,
	chEleSides,
	chEleSides,
	chEleBar
};
const char chSeven[CH_SIZE][CH_SIZE] = {
	chEleBar,
	chEleSides,
	chEleSides,
	chEleSides,
	chEleRight,
	chEleRight,
	chEleRight
};
const char chEight[CH_SIZE][CH_SIZE] = {
	chEleBar,
	chEleSides,
	chEleSides,
	chEleBar,
	chEleSides,
	chEleSides,
	chEleBar
};
const char chNine[CH_SIZE][CH_SIZE] = {
	chEleBar,
	chEleSides,
	chEleSides,
	chEleBar,
	chEleRight,
	chEleRight,
	chEleRight
};

#define chAlbS chFive
const char chAlbO[CH_SIZE][CH_SIZE] = {
	chEleBar,
	chEleSides,
	chEleSides,
	chEleSides,
	chEleSides,
	chEleSides,
	chEleBar
};
const char chAlbN[CH_SIZE][CH_SIZE] = {
	chEleSides,
	{'*', '*', '#', '#', '#', '#', '*'},
	{'*', '#', '*', '#', '#', '#', '*'},
	{'*', '#', '#', '*', '#', '#', '*'},
	{'*', '#', '#', '#', '*', '#', '*'},
	{'*', '#', '#', '#', '#', '*', '*'},
	chEleSides
};
const char chAlbI[CH_SIZE][CH_SIZE] = {
	{'#', '#', '*', '*', '*', '#', '#'},
	chEleMiddle,
	chEleMiddle,
	chEleMiddle,
	chEleMiddle,
	chEleMiddle,
	{'#', '#', '*', '*', '*', '#', '#'}
};
const char chAlbG[CH_SIZE][CH_SIZE] = {
	chEleBar,
	chEleLeft,
	chEleLeft,
	{'*', '#', '#', '*', '*', '*', '*'},
	chEleSides,
	chEleSides,
	chEleBar
};
const char chAlbT[CH_SIZE][CH_SIZE] = {
	chEleBar,
	chEleMiddle,
	chEleMiddle,
	chEleMiddle,
	chEleMiddle,
	chEleMiddle,
	chEleMiddle
};
const char chAlbP[CH_SIZE][CH_SIZE] = {
	chEleBar,
	chEleSides,
	chEleSides,
	chEleBar,
	chEleLeft,
	chEleLeft,
	chEleLeft
};
const char chAlbA[CH_SIZE][CH_SIZE] = {
	chEleBar,
	chEleSides,
	chEleSides,
	chEleBar,
	chEleSides,
	chEleSides,
	chEleSides
};
const char chAlbR[CH_SIZE][CH_SIZE] = {
	chEleBar,
	chEleSides,
	chEleSides,
	chEleBar,
	{'*', '#', '#', '*', '*', '#', '#'},
	{'*', '#', '#', '#', '*', '*', '#'},
	{'*', '#', '#', '#', '#', '*', '*'}
};
const char chAlbY[CH_SIZE][CH_SIZE] = {
	chEleSides,
	{'#', '*', '#', '#', '#', '*', '#'},
	{'#', '#', '*', '#', '*', '#', '#'},
	chEleMiddle,
	chEleMiddle,
	chEleMiddle,
	chEleMiddle
};
const char chAlbD[CH_SIZE][CH_SIZE] = {
	chEleBar,
	chEleSides,
	chEleSides,
	chEleSides,
	chEleSides,
	{'*', '#', '#', '#', '#', '*', '#'},
	{'*', '*', '*', '*', '*', '#', '#'}
};
const char chAlbC[CH_SIZE][CH_SIZE] = {
	chEleBar,
    chEleLeft,
	chEleLeft,
	chEleLeft,
	chEleLeft,
	chEleLeft,
	chEleBar
};
const char chAlbE[CH_SIZE][CH_SIZE] = {
	chEleBar,
    chEleLeft,
	chEleLeft,
	chEleBar,
	chEleLeft,
	chEleLeft,
	chEleBar
};
const char chAlbW[CH_SIZE][CH_SIZE] = {
	{'*', '#', '#', '*', '#', '#', '*'},
	{'*', '#', '#', '*', '#', '#', '*'},
	{'*', '#', '#', '*', '#', '#', '*'},
	{'*', '#', '#', '*', '#', '#', '*'},
	{'*', '#', '#', '*', '#', '#', '*'},
	{'*', '#', '#', '*', '#', '#', '*'},
	chEleBar
};
const char chAlbL[CH_SIZE][CH_SIZE] = {
	chEleLeft,
    chEleLeft,
	chEleLeft,
	chEleLeft,
	chEleLeft,
	chEleLeft,
	chEleBar
};
const char chAlbH[CH_SIZE][CH_SIZE] = {
	chEleSides,
	chEleSides,
	chEleSides,
	chEleBar,
	chEleSides,
	chEleSides,
	chEleSides
};
const char chAlbU[CH_SIZE][CH_SIZE] = {
	chEleSides,
	chEleSides,
	chEleSides,
	chEleSides,
	chEleSides,
	chEleSides,
	chEleBar
};
const char chQues[CH_SIZE][CH_SIZE] = {
	chEleBar,
    chEleRight,
	{'#', '#', '#', '*', '*', '*', '*'},
	chEleMiddle,
	chEleMiddle,
	{'#', '#', '#', '#', '#', '#', '#'},
	chEleMiddle
};
const char chExcl[CH_SIZE][CH_SIZE] = {
	chEleMiddle,
	chEleMiddle,
	chEleMiddle,
	chEleMiddle,
	chEleMiddle,
	{'#', '#', '#', '#', '#', '#', '#'},
	chEleMiddle
};


void renderCover(bool clear_screen, int chose_rect, int color){
	if(clear_screen == TRUE) clearScreen();

	int i, x, y;
	for(i = 0; i < rectPoolSize; i++){
		GameRect *rect = gameRects[i];
		if(rect -> coverVisible == FALSE) continue;
		int rectColor = (chose_rect == i)? color : (rect -> color);
		int startX = rect -> col,
			startY = rect -> row,
			size = rect -> size;

		for(y = 0; y < size; y++){
			for(x = 0; x < size; x++){
				putASCII2(startX + x, startY + y, ' ', COLOR_BLACK, rectColor);
			}
		}
	}
	drawCmdWindow();
}

#define printChArray(INDEX)	\
for(y = 0; y < CH_SIZE; y++){	\
	for(x = 0; x < CH_SIZE; x++){	\
		if( ch##INDEX[y][x] == '#' ){	\
			putASCII2(startX + x, startY + y, ' ', COLOR_BLACK, COLOR_BLACK);	\
		}else /*if(chArray[y][x] == '*')*/{	\
			putASCII2(startX + x, startY + y, ' ', COLOR_BLACK, rectColor);	\
		}	\
	}	\
}

void renderVal(bool clear_screen){
	if(clear_screen == TRUE) clearScreen();

	int i, x, y;
	for(i = 0; i < rectPoolSize; i++){
		GameRect *rect = gameRects[i];
		int rectColor = rect -> color;
		int startX = rect -> col,
			startY = rect -> row;

		switch(rect -> val){
			//printw("Value: %d\n", rect -> val);
			case 1:
			printChArray(One);
			break;

			case 2:
			printChArray(Two);
			break;

			case 3:
			printChArray(Three);
			break;

			case 4:
			printChArray(Four);
			break;

			case 5:
			printChArray(Five);
			break;

			case 6:
			printChArray(Six);
			break;

			case 7:
			printChArray(Seven);
			break;

			case 8:
			printChArray(Eight);
			break;

			case 9:
			printChArray(Nine);
			break;
		}
	}
	drawCmdWindow();
}

void setCoversVisibled(){
	int i;
	GameRect *rect;
	for(i = 0; i < rectPoolSize; i++){
		rect = gameRects[i];
		rect -> coverVisible = TRUE;
	}
}

int renderString(const char *str, int start_x, int start_y, int color){
	int startX = start_x,
		startY = start_y,
		rectColor = color;
	int i, x, y, len = strlen(str);

	for(i = 0; i < len; i++){
		switch(str[i]){
			case '1':
			printChArray(One);
			break;

			case '2':
			printChArray(Two);
			break;

			case '3':
			printChArray(Three);
			break;

			case '4':
			printChArray(Four);
			break;

			case '5':
			printChArray(Five);
			break;

			case '6':
			printChArray(Six);
			break;

			case '7':
			printChArray(Seven);
			break;

			case '8':
			printChArray(Eight);
			break;

			case '9':
			printChArray(Nine);
			break;

			case 's':
			case 'S':
			printChArray(AlbS);
			break;

			case 't':
			case 'T':
			printChArray(AlbT);
			break;

			case 'a':
			case 'A':
			printChArray(AlbA);
			break;

			case 'c':
			case 'C':
			printChArray(AlbC);
			break;

			case 'l':
			case 'L':
			printChArray(AlbL);
			break;

			case 'w':
			case 'W':
			printChArray(AlbW);
			break;

			case 'r':
			case 'R':
			printChArray(AlbR);
			break;

			case 'e':
			case 'E':
			printChArray(AlbE);
			break;

			case 'd':
			case 'D':
			printChArray(AlbD);
			break;

			case 'y':
			case 'Y':
			printChArray(AlbY);
			break;

			case '0':
			case 'o':
			case 'O':
			printChArray(AlbO);
			break;

			case 'n':
			case 'N':
			printChArray(AlbN);
			break;

			case 'p':
			case 'P':
			printChArray(AlbP);
			break;

			case 'i':
			case 'I':
			printChArray(AlbI);
			break;

			case 'g':
			case 'G':
			printChArray(AlbG);
			break;

			case 'h':
			case 'H':
			printChArray(AlbH);
			break;

			case 'u':
			case 'U':
			printChArray(AlbU);
			break;

			case '?':
			printChArray(Ques);
			break;

			case '!':
			printChArray(Excl);
			break;

			default:
			continue;
		}
		startX += CH_SIZE;
        startX += CH_PADDING;
	}
	drawCmdWindow();

	return startX;
}