#include <jni.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "main.h"

jclass gClassString = NULL;

#define RECT_COLOR_DEFAULT	COLOR_WHITE

void initWrapper(JNIEnv *env, jobject thiz){
	_init();
}
void endWrapper(JNIEnv *env, jobject thiz){
	_end();

	//Recycle jni references
	if(gClassString != NULL){
		(*env) -> DeleteGlobalRef(env, gClassString);
	}
}
void waitKeyWrapper(JNIEnv *env, jobject thiz){
	wait_key();
}

//Window part
void welcomeWindowWrapper(JNIEnv *env, jobject thiz){
	windowWelcome();
}
jstring connectWindowWrapper(JNIEnv *env, jobject thiz){
	char serverAddr[26];
	windowConnect(serverAddr);

	return (*env) -> NewStringUTF(env, (const char*)serverAddr);
}
jstring userNameWindowWrapper(JNIEnv *env, jobject thiz){
	char userName[11];
	windowUsername(userName);

	return (*env) -> NewStringUTF(env, (const char*)userName);
}
void playersWindowWrapper(JNIEnv *env, jobject thiz, jint requireNum, jfloat timeLimitSec, jobjectArray players){
	int playerNum = ((*env) -> GetArrayLength(env, players));
	if(playerNum <= 0) return;

	char *playersNative[playerNum];
	const char *tmp;
	int i;
	jstring name;
	for(i = 0; i < playerNum; i++){
		name = (jstring)((*env) -> GetObjectArrayElement(env, players, i));
		if(name == NULL) continue;
		tmp = (*env) -> GetStringUTFChars(env, name, JNI_FALSE);

		playersNative[i] = (char*)malloc(sizeof(char) * NAME_LENGTH_MAX);
		strncpy(playersNative[i], tmp, NAME_LENGTH_MAX - 1);
		playersNative[i][NAME_LENGTH_MAX - 1] = '\0';

		(*env) -> ReleaseStringUTFChars(env, name, tmp);
	}

	windowShowPlayers(requireNum, (float)timeLimitSec, playersNative, playerNum);
	for(i = 0; i < playerNum; i++){
		if(playersNative[i] != NULL) free(playersNative[i]);
	}
}
void addRectWrapper(JNIEnv *env, jobject thiz, jint col, jint row, jint size, jint val){
	GameRect rect;
	rect.row = row;
	rect.col = col;
	rect.size = size;
	rect.val = val;
	rect.color = RECT_COLOR_DEFAULT;
	rect.coverVisible = TRUE;

	addRect(rect);
}
void clearRectsWrapper(JNIEnv *env, jobject thiz){
	clearRects();
}
void coverVisibleWrapper(JNIEnv *env, jobject thiz){
	setCoversVisibled();
}
void play1Wrapper(JNIEnv *env, jobject thiz){
	windowPlay1();
}
int play2Wrapper(JNIEnv *env, jobject thiz){
	return windowPlay2();
}
void scoreWindowWrapper(JNIEnv *env, jobject thiz, jint score){
	windowScore((int)score);
}

//Info Part
void infoErrorWrapper(JNIEnv *env, jobject thiz, jstring msg){
	const char *str = (*env) -> GetStringUTFChars(env, msg, JNI_FALSE);
	infoError(str);
	(*env) -> ReleaseStringUTFChars(env, msg, str);
}
void infoNormalWrapper(JNIEnv *env, jobject thiz, jstring msg){
	const char *str = (*env) -> GetStringUTFChars(env, msg, JNI_FALSE);
    infoNormal(str);
    (*env) -> ReleaseStringUTFChars(env, msg, str);
}
void infoGameStartWrapper(JNIEnv *env, jobject thiz){
    infoGameStart();
}
void infoResultWrapper(JNIEnv *env, jobject thiz, jint resultCode){
	int result = (int)resultCode;
	switch(result){
		case 0: //AC
		infoResultAC();
		break;

		case 1: //WA
		infoResultWA();
		break;

		case 2: //TLE
		infoResultTLE();
		break;
	}
}

static JNINativeMethod jniMethods[] = {
	{"initNative", "()V", (void*)initWrapper},
	{"cleanNative", "()V", (void*)endWrapper},
	{"waitForKey", "()V", (void*)waitKeyWrapper},

	{"showWelcomeNative", "()V", (void*)welcomeWindowWrapper},
	{"showNumbersNative", "()V", (void*)play1Wrapper},
	{"setCoverVisibleNative", "()V", (void*)coverVisibleWrapper},
	{"playWindowNative", "()I", (void*)play2Wrapper},
	{"addRectNative", "(IIII)V", (void*)addRectWrapper},
	{"clearRectsNative", "()V", (void*)clearRectsWrapper},
	{"connectWindowNative", "()Ljava/lang/String;", (void*)connectWindowWrapper},
	{"userNameWindowNative", "()Ljava/lang/String;", (void*)userNameWindowWrapper},
	{"playerListWindowNative", "(IF[Ljava/lang/String;)V", (void*)playersWindowWrapper},
	{"scoreWindowNative", "(I)V", (void*)scoreWindowWrapper},

	{"showInfoErrorNative", "(Ljava/lang/String;)V", (void*)infoErrorWrapper},
	{"showInfoNative", "(Ljava/lang/String;)V", (void*)infoNormalWrapper},
	{"showGameStartNative", "()V", (void*)infoGameStartWrapper},
	{"showResultNative", "(I)V", (void*)infoResultWrapper}
};

JNIEXPORT
jint JNICALL JNI_OnLoad(JavaVM *vm, void *reserved){

	JNIEnv *env = NULL;
	if((*vm) -> GetEnv(vm, (void**)&env, JNI_VERSION_1_4) != JNI_OK){
		fprintf(stderr, "Error getting jni environment");
		return JNI_ERR;
	}

	//Find bind class
	jclass classBind = (*env) -> FindClass(env, JNI_BIND_CLASS_NAME);
	if(classBind == NULL){
		fprintf(stderr, "Error getting class: "JNI_BIND_CLASS_NAME);
		return JNI_ERR;
	}
	//Find String class
	jclass classStringLocal = (*env) -> FindClass(env, "java/lang/String");
	if(classStringLocal != NULL){
		//Make it global
		gClassString = (*env) -> NewGlobalRef(env, classStringLocal);
	}else{
		fprintf(stderr, "Error getting class: java/lang/String");
	}

	//Register native methods
	//printf("jni methods num: %d\n", ARRAY_SIZE(jniMethods));
	if( (*env) -> RegisterNatives(env, classBind, jniMethods, ARRAY_SIZE(jniMethods)) < 0){
		fprintf(stderr, "Error registering native methods");
		return JNI_ERR;
	}

	return JNI_VERSION_1_4;
}