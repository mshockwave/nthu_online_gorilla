#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <signal.h>

#include <ncurses.h>
#include <form.h>

#include <console_draw2.h>
#include <kb_input2.h>
#include <console_util.h>
#include <game_util.h>

Configs gConfigs;
bool gSupportColor = FALSE;

extern char gIconGorillaNormal[ICON_SIZE_MAX][ICON_SIZE_MAX];
extern char gIconGorillaHappy[ICON_SIZE_MAX][ICON_SIZE_MAX];
extern char gIconGorillaMood[ICON_SIZE_MAX][ICON_SIZE_MAX];

void _init(){
	/*ncurses init part*/
	initscr();
	cbreak();
	noecho();
	keypad(stdscr, TRUE);
	if( (gSupportColor = has_colors()) == TRUE){
		//Color init part
		start_color();
	}

	/*game init part*/
	initRectsPool();
	initColorStorage();
	initIcons();
}
void wait_key(){
	getch();
}
void _end(){
	destroyRects();
	destroyColorStorage();
	endwin();
}

#define TITLE_NTHU	"NTHU"
#define TITLE_ONLINE	"Online"
#define TITLE_GORILLA	"Gorillas"
#define TITLE_PLAYERS	"Players"

void renderBanner(char icon[ICON_SIZE_MAX][ICON_SIZE_MAX], int strColor, int iconColor){
	int offsetX = ( DEFAULT_WIDTH
					- strlen(TITLE_GORILLA) * (CH_SIZE + CH_PADDING)
					- strlen(TITLE_ONLINE) * (CH_SIZE + CH_PADDING)
					- getIconWidth(icon) ) / 2 + CH_SIZE;

	offsetX = renderString(TITLE_ONLINE, offsetX, 0, strColor);
	offsetX = renderIcon(icon, offsetX, 0, iconColor);
	renderString(TITLE_GORILLA, offsetX, 0, strColor);
	drawCmdWindow();
}

/*Window part*/
void windowWelcome(){
	int len = strlen(TITLE_NTHU);
	len = (CH_SIZE + CH_PADDING) * len - CH_PADDING;
	renderString(TITLE_NTHU, (DEFAULT_WIDTH - len) / 2, CH_PADDING, COLOR_BLUE);

	len = strlen(TITLE_ONLINE);
    len = (CH_SIZE + CH_PADDING) * len - CH_PADDING;
    renderString(TITLE_ONLINE, (DEFAULT_WIDTH - len) / 2, CH_PADDING * 2 + CH_SIZE, COLOR_CYAN);

    len = strlen(TITLE_GORILLA);
    len = (CH_SIZE + CH_PADDING) * len - CH_PADDING;
    renderString(TITLE_GORILLA, (DEFAULT_WIDTH - len) / 2, CH_PADDING * 3 + CH_SIZE * 2, COLOR_CYAN);

    printMiddleHor("By Bekket McClane", CH_PADDING * 4 + CH_SIZE * 3, COLOR_WHITE);
    printMiddleHor("Press Any Key To Start", CH_PADDING * 5 + CH_SIZE * 3, COLOR_YELLOW);

	getch();
}
void windowConnect(char *serverAddr){
	clear();

	//ncurses form
	FORM *form;
	FIELD *field[2];
	int colorMain = getColor(COLOR_BLACK, COLOR_WHITE);
	const int ENTER_FIELD_WIDTH = 25;
	const char *text = "Enter Server Address: ";
	int formX = (DEFAULT_WIDTH - ENTER_FIELD_WIDTH - strlen(text)) / 2,
		formY = (DEFAULT_HEIGHT) / 2;

	field[0] = new_field(1, ENTER_FIELD_WIDTH, formY, formX + strlen(text), 0, 0);
	set_field_back(field[0], COLOR_PAIR(colorMain));
	set_field_fore(field[0], COLOR_PAIR(colorMain));
	field[1] = NULL;

	form = new_form(field);
	post_form(form);
	drawCmdWindow();

	mvprintw(formY, formX, text);
	drawCmdWindow();

	//Title
	renderBanner(gIconGorillaNormal, COLOR_YELLOW, COLOR_WHITE);

	int ch, chCount = 0;;
	while( (ch = getch()) != KEY_ENTER && ch != '\n'){
		if(ch == KEY_BACKSPACE || ch == KEY_DC || ch == KEY_DL || ch == KEY_LEFT){
			form_driver(form, REQ_DEL_PREV);
			if(chCount >= 1) chCount--;
		}else{
			form_driver(form, ch);
			if(chCount + 1 <= ENTER_FIELD_WIDTH) chCount++;
		}
		form_driver(form, REQ_VALIDATION);
	}

	char buf[ENTER_FIELD_WIDTH + 1];
	strncpy(buf, field_buffer(field[0], 0), ENTER_FIELD_WIDTH);
	int i;
	for(i = 0; i < chCount; i++){
		serverAddr[i] = buf[i];
	}
	serverAddr[chCount] = '\0';

	unpost_form(form);
	free_form(form);
	free_field(field[0]);

	clear();
}
void windowUsername(char *userName){
	clear();

	//ncurses form
	FORM *form;
	FIELD *field[2];
	int colorMain = getColor(COLOR_BLACK, COLOR_WHITE);
	const int ENTER_FIELD_WIDTH = 10;
	const char *text = "Enter Nick Name: ";
	int formX = (DEFAULT_WIDTH - ENTER_FIELD_WIDTH - strlen(text)) / 2,
		formY = (DEFAULT_HEIGHT) / 2;

	field[0] = new_field(1, ENTER_FIELD_WIDTH, formY, formX + strlen(text), 0, 0);
	set_field_back(field[0], COLOR_PAIR(colorMain));
	set_field_fore(field[0], COLOR_PAIR(colorMain));
	field[1] = NULL;

	form = new_form(field);
	post_form(form);
	drawCmdWindow();

	mvprintw(formY, formX, text);
	drawCmdWindow();

	//Title
	renderBanner(gIconGorillaNormal, COLOR_YELLOW, COLOR_WHITE);

	int ch, chCount = 0;
	while( (ch = getch()) != KEY_ENTER && ch != '\n'){
		if(ch == KEY_BACKSPACE || ch == KEY_DC || ch == KEY_DL || ch == KEY_LEFT){
			form_driver(form, REQ_DEL_PREV);
			if(chCount >= 1) chCount--;
		}else{
			form_driver(form, ch);
			if(chCount + 1 <= ENTER_FIELD_WIDTH) chCount++;
		}
		form_driver(form, REQ_VALIDATION);
	}

	char buf[ENTER_FIELD_WIDTH + 1];
	strncpy(buf, field_buffer(field[0], 0), ENTER_FIELD_WIDTH);
	int i;
	for(i = 0; i < chCount; i++){
		userName[i] = buf[i];
	}
	userName[chCount] = '\0';

	unpost_form(form);
	free_form(form);
	free_field(field[0]);

	clear();
}
extern const int NAME_LENGTH_MAX;
#define PLAYER_INFO_BOX_HEIGHT	4 //Must be even number
#define PLAYER_INFO_BOX_WIDTH	(NAME_LENGTH_MAX+1)
int gIndex = 0;
void windowShowPlayers(int requireNum, float timeLimitSec, char *players[], int playersNum){
	int titleLen = strlen(TITLE_PLAYERS);
	int startX = (DEFAULT_WIDTH - titleLen * (CH_SIZE + CH_PADDING)) / 2;

	const char *listInstruction = "(Press any key to flip list)";
	int instructLen = strlen(listInstruction);

	int offsetX = (DEFAULT_WIDTH - PLAYER_INFO_BOX_WIDTH) / 2;
	int offsetH = (CH_SIZE + CH_PADDING + 1), h;
	int i, j, ch, c;
	int iconOffsetX = (offsetX - getIconWidth(gIconGorillaHappy)) / 2,
		iconOffsetY = (DEFAULT_HEIGHT - getIconHeight(gIconGorillaHappy)) / 2;
	bool out;

	clear();

	renderIcon(gIconGorillaHappy, iconOffsetX, iconOffsetY, COLOR_GREEN);

	renderString(TITLE_PLAYERS, startX, CH_PADDING, COLOR_MAGENTA);
	drawCmdWindow();

	out = FALSE;
	for(i = 0; (h = (i + offsetH)) <= DEFAULT_HEIGHT; i++){
		switch(i % PLAYER_INFO_BOX_HEIGHT){
			case 0: //Tail, print dashes
			putASCII2(offsetX, h, '+', COLOR_YELLOW, COLOR_BLACK);
			for(j = 1; j < PLAYER_INFO_BOX_WIDTH - 1; j++){
				putASCII2(offsetX + j, h, '-', COLOR_YELLOW, COLOR_BLACK);
			}
			putASCII2(offsetX + PLAYER_INFO_BOX_WIDTH - 1, h, '+', COLOR_YELLOW, COLOR_BLACK);
			if(gIndex >= playersNum) out = TRUE;
			break;

			case ((PLAYER_INFO_BOX_HEIGHT - 1) / 2 + 1): //Middle, print text
			printMiddleHor(players[gIndex], h, COLOR_WHITE);
			gIndex++;
			break;
		}
		if(out == TRUE) break;
	}
	gIndex %= playersNum;

	//Print some extra stuff
	c = getColor(COLOR_BLACK, COLOR_CYAN);
	attron(COLOR_PAIR(c));
	mvprintw(offsetH, (DEFAULT_WIDTH + PLAYER_INFO_BOX_WIDTH) / 2 + 10, " Current/Require Players: %d/%d ", playersNum, requireNum);
	attroff(COLOR_PAIR(c));

	c = getColor(COLOR_WHITE, COLOR_RED);
	attron(COLOR_PAIR(c));
	mvprintw(offsetH + 2, (DEFAULT_WIDTH + PLAYER_INFO_BOX_WIDTH) / 2 + 10, " Time limit: %.2f sec(s) ", timeLimitSec);
	attroff(COLOR_PAIR(c));

	for(i = 0; i < instructLen; i++){
		putASCII2( (DEFAULT_WIDTH + PLAYER_INFO_BOX_WIDTH) / 2 + 10 + i, offsetH + 4, listInstruction[i], COLOR_WHITE, COLOR_BLACK );
	}
	drawCmdWindow();
}
int windowPlay(){
	GameRect **rects, *rect;
	int num = getRectsNum();
	int ch, i;
	int counter = 0;
	bool isWa = FALSE;

	for(i = 0; i < num; i++){
		rect = getRect(i);
		renderVal(TRUE);
		renderCover(FALSE, i, COLOR_CYAN);

		ch = getch();
		if((ch - '0') == (rect -> val)){ //answer correct
			rect -> coverVisible = FALSE;
			counter++;
		}else{ //wrong answer
			isWa = TRUE;
		}

		renderVal(TRUE);
		renderCover(FALSE, (isWa == TRUE)? i : -1, COLOR_RED);
		if(isWa == TRUE){
			pause(300);
			break;
		}
	}
#ifdef DEBUG_EXEC
	pause(300);
#endif
	return counter;
}
int windowPlay2(void){
	return windowPlay();
}
void windowScore(int score){
	clear();

	const int BUF_MAX = DEFAULT_WIDTH / CH_SIZE + 1;
	char numArray[BUF_MAX];

	snprintf(numArray, sizeof(char) * BUF_MAX, "%d", score);
	int scoreLen = strlen(numArray);

	printMiddleHor("Total Score: ", (DEFAULT_HEIGHT - CH_SIZE) / 2 - 2, COLOR_CYAN);

	int scoreX = (DEFAULT_WIDTH - scoreLen * 7) / 2,
		scoreY = (DEFAULT_HEIGHT - CH_SIZE) / 2;
	renderString(numArray, scoreX, scoreY, COLOR_YELLOW);
	drawCmdWindow();

	getch();
}

/*Info window part*/
void infoNormal(const char *msg){
	clear();
	printMiddle(msg, COLOR_CYAN);
}
void infoError(const char *errorMsg){
	clear();
	printMiddle(errorMsg, COLOR_RED);
}
void infoGameStart(){
	const char *str1 = "Ready?",
				*str2 = "Start!!";
	int len = strlen(str1);
	len = (CH_SIZE + CH_PADDING) * len - CH_PADDING;
	clear();
	renderString(str1, (DEFAULT_WIDTH - len) / 2, (DEFAULT_HEIGHT - CH_SIZE) / 2, COLOR_CYAN);
	//renderString(str1, 0, 0, COLOR_CYAN);
	//getch();
	pause(1200);
	clear();
	len = strlen(str2);
	len = (CH_SIZE + CH_PADDING) * len - CH_PADDING;
    renderString(str2, (DEFAULT_WIDTH - len) / 2, (DEFAULT_HEIGHT - CH_SIZE) / 2, COLOR_YELLOW);
    //getch();
    pause(800);
}
#define RESULT_PAUSE_SEC	3
#define DECLARE_INFO_RESULT(RESULT, COLOR_CAP)	\
void infoResult##RESULT(){	\
	const char *str = TO_STR(RESULT);	\
	char infoBuffer[30];	\
	int len = strlen(str);	\
	len = (CH_SIZE + CH_PADDING) * len - CH_PADDING;	\
	\
	int counter = RESULT_PAUSE_SEC;	\
	while(counter > 0){	\
		clear();	\
		snprintf(infoBuffer, sizeof(char) * 30, "%d sec(s) until next stage", counter);	\
        printMiddleHor(infoBuffer, (DEFAULT_HEIGHT + CH_SIZE) / 2 + CH_PADDING, COLOR_WHITE);	\
        renderString(str, (DEFAULT_WIDTH - len) / 2, (DEFAULT_HEIGHT - CH_SIZE) / 2, COLOR_##COLOR_CAP);	\
		\
		counter--;	\
		pause(1000);	\
	}	\
}

DECLARE_INFO_RESULT(AC, GREEN);
DECLARE_INFO_RESULT(WA, RED);
DECLARE_INFO_RESULT(TLE, MAGENTA);

#ifdef DEBUG_EXEC
int main(){
	_init();

	/*
	GameRect one, two, three;
	one.coverVisible = two.coverVisible = three.coverVisible = TRUE;
	one.size = two.size = three.size = 7;
	one.color = two.color = three.color = COLOR_WHITE;
	one.val = 1;
	two.val = 2;
	three.val = 3;

	one.col = 60;
	one.row = 20;
	two.col = 0;
	two.row = 30;
	three.col = 0;
	three.row = 0;

	addRect(three);
	addRect(two);
	addRect(one);

	getch();
	renderVal(TRUE);
	pause(300);
	windowPlay();
	//getch();
	setCoversVisibled();
	*/
	windowWelcome();
	const char *players[] = {
		"Hello",
		"This is test",
		"This is test",
		"This is test",
		"This is test",
		"This is test",
		"This is test",
		"This is test",
		"This is test",
		"This is test",
		"This is test",
		"This is test",
		"This is test",
		"This is test",
		"This is test",
		"This is test"
	};
	windowShowPlayers(1, 0.0, (char**)players, 16);

	_end();

	return 0;
}
#endif