#ifndef _MAIN_H
#define _MAIN_H

#include <game_util.h>

#define JNI_BIND_CLASS_NAME	"nthu_ogs/NativeProvider"

const int NAME_LENGTH_MAX = 44;

/*APIs that exposed to jni wrapper*/
//Main part
void _init(void);
void _end();
void wait_key(void);

//window and info screen part
void windowWelcome(void);
void windowConnect(char*);
void windowUsername(char*);
void windowShowPlayers(int, float, char**, int);
void windowPlay1(void){
	renderVal(TRUE);
}
int windowPlay2(void);
void windowScore(int);

void infoNormal(const char*);
void infoError(const char*);
void infoGameStart(void);
void infoResultAC(void);
void infoResultWA(void);
void infoResultTLE(void);

#endif