#include <ncurses.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <console_draw2.h>
#include <console_util.h>
#include <kb_input2.h>

extern bool gSupportColor;

int getColor(int, int);

void putASCII2( int col, int row, int ch, int fgcolor, int bgcolor ){
	if( gSupportColor == TRUE){
		int colorIndex = getColor(fgcolor, bgcolor);
		attron(COLOR_PAIR(colorIndex));
		mvaddch(row, col, ch);
		attroff(COLOR_PAIR(colorIndex));
	}else{
		mvaddch(row, col, ch);
	}
}
void drawCmdWindow(){
	refresh();
}
void clearScreen(){
	clear();
}

void printMiddleHor(const char *str, int y, int color){
	int i, len = strlen(str), startCol = DEFAULT_WIDTH / 2 - len / 2;

	for(i = 0; i < len; i++){
		putASCII2(startCol + i, y, str[i], color, COLOR_BLACK);
	}
	drawCmdWindow();
}
//void printMiddleVer(const char *str, int x, int color);
void printMiddle(const char *str, int color){
	printMiddleHor(str, DEFAULT_HEIGHT / 2, color);
}

/*Color storage*/
typedef struct _color_pair {
	struct _color_pair *next;
	int index;
	int fg, bg;
} ColorPair;
ColorPair *colorStorageHead = NULL, *colorStorageTail = NULL;

void initColorStorage(){
	if(gSupportColor == TRUE && 
		colorStorageHead == NULL && colorStorageTail == NULL){
		colorStorageHead = (ColorPair*)malloc(sizeof(ColorPair));
		colorStorageHead -> next = NULL;
		colorStorageTail = colorStorageHead;
		colorStorageHead -> index = 0;
	}
}
void destroyColorStorage(){
	if(gSupportColor == TRUE &&
		colorStorageHead != NULL && colorStorageTail != NULL){
		colorStorageTail = NULL;

		ColorPair *color_pair;
		for(color_pair = colorStorageHead -> next; color_pair != NULL; color_pair = color_pair -> next ){
			free(color_pair);
		}
		free(colorStorageHead);
	}
}
int getColor(int fg, int bg){
	ColorPair *color_pair;
	int index = -1;
	for(color_pair = colorStorageHead -> next; color_pair != NULL; color_pair = color_pair -> next ){
		if(color_pair -> fg == fg && color_pair -> bg == bg){
			index = color_pair -> index;
			break;
		}
	}
	if(index >= 1) return index;

	//Add new color
	color_pair = (ColorPair*)malloc(sizeof(ColorPair));
	color_pair -> fg = fg;
	color_pair -> bg = bg;
	color_pair -> index = (colorStorageTail -> index) + 1;
	init_pair(color_pair -> index, fg, bg);

	colorStorageTail -> next = color_pair;
	color_pair -> next = NULL;
	colorStorageTail = color_pair;

	return (color_pair -> index);
}

/*Keyboard part*/
void initializeKeyInput(void){
	noecho();
	cbreak();
	keypad(stdscr, TRUE);
}

/*Not supported by ncurses*/
int waitForKeyDown(float sec){ return 0; }
int getKeyEventASCII(void){ return 0; }
int getKeyEventVirtual(void){ return 0; }

void pause(long millis){
	int startClock = clock();
	while( ((float)(clock() - startClock))/CLOCKS_PER_SEC * 1000 < millis );
}
void delay(void){
	pause( (long)(1000 / gFrameRate) );
}