#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <windows.h>
#include <mmsystem.h>
/* 音效記得要加入這個 */
#include "audio.h"
/* 另外還有底下的設定 */
/* For sound effect: In [Project Options]->[Parameters]->[Linker] add the parameter -lwinmm */

#include "console_draw2.h"
#include "kb_input2.h"

/*
putASCII2()  最後兩個參數分別是字元前景和背景的顏色
可以參考下面的顏色值對照表來設定你想要的顏色
   0: 黑     1: 暗藍   2: 暗綠   3: 暗青
   4: 暗紅   5: 暗紫   6: 暗黃   7: 淺灰
   8: 暗灰   9: 亮藍  10: 亮綠  11: 亮青
  12: 亮紅  13: 亮紫  14: 亮黃  15: 白
*/

#define WIDTH     30
#define HEIGHT    20
#define OFFSET_X  2
#define OFFSET_Y  2

#define MAX_LENGTH 100
#define REFRESH_DURATION 5

struct t_SnakeNode {
   int x;
   int y;
   int color;
   int direction;
   int duration;
   int tick;
};
typedef struct t_SnakeNode SnakeNode;


void delay(float sec);
void putString(int x, int y, char *p, int fg_color, int bg_color);

void genSnake(SnakeNode *sp, int length);
void showSnake(SnakeNode *sp, int length);
void moveSnake(SnakeNode *sp, int length);
int addSnake(SnakeNode *sp, int length);
void shiftRight(SnakeNode *sp, int length);
void shiftLeft(SnakeNode *sp, int length);

void genFood(SnakeNode *fp);
void showFood(SnakeNode *fp);
void moveFood(SnakeNode *fp);
int eat(SnakeNode *sp, int length, SnakeNode *fp);

int main(void)
{
    int IsEnding = 0;
    char logo[] = "SAMPLE PROGRAM PROVIDED BY I2P12";

    SnakeNode snake[MAX_LENGTH];
    int length = 15;

    SnakeNode food[1];

    clock_t startc;
    int game_tick = 0;

    int vk;

    Audio au1;
    openAudioFile("chimes.wav", &au1);

    startc = clock();

    initializeKeyInput();

    genSnake(snake, length);
    genFood(food);

    while (!IsEnding) { /* game loop */

        if ((double)(clock()-startc) > 0.01*CLOCKS_PER_SEC) {
            game_tick++;
            startc = clock();
        }


        if (game_tick > REFRESH_DURATION) {
            game_tick = 0;
            clearScreen();

            showSnake(snake, length);

            if (eat(snake, length, food)) {
                length = addSnake(snake, length);
                playAudio(&au1);
                genFood(food);
            }
            showFood(food);

            putString(OFFSET_X, OFFSET_Y-2, logo, 14, 3);

            drawCmdWindow();  /* update window immediately */
        }


        moveFood(food);
        moveSnake(snake, length);

        if(waitForKeyDown(0.01)) {
            vk=getKeyEventVirtual();	/* read a virtual key */

            switch(vk) {
                case VK_ESCAPE:  /* 按下 ESC 鍵 就結束 */
                    IsEnding = 1;
                    break;
                case VK_LEFT:
                case VK_J:
                    snake[0].direction = 2;
                    break;
                case VK_RIGHT:
                case VK_L:
                    snake[0].direction = 0;
                    break;
                case VK_DOWN:
                case VK_K:
                    snake[0].direction = 1;
                    break;
                case VK_UP:
                case VK_I:
                    snake[0].direction = 3;
                    break;
                case VK_P:
                	shiftRight(snake, length);
                	break;
                case VK_O:
                	shiftLeft(snake, length);
                	break;                	
            }
        }

    } /* while (IsEnding) */


    return 0;
}




void delay(float sec)
{
    clock_t startc;
    startc = clock();
    for ( ; ; ) {
        if ((float)(clock()-startc)/CLOCKS_PER_SEC > sec) break;
    }
}

void putString(int x, int y, char *p, int fg_color, int bg_color)
{
    int i;
    for(i=0; i<strlen(p); i++) {
        putASCII2(x+i, y, p[i], fg_color, bg_color);
    }
}


/****************************************************************************************************/

int eat(SnakeNode *sp, int length, SnakeNode *fp)
{
    int k;
    /* 0: right, 1: down, 2: left, 3: up */
    for (k=0; k<length; k++) {
        if (sp[k].x == fp[0].x &&  sp[k].y == fp[0].y)
        return 1;
    }
    return 0;
}

void genFood(SnakeNode *fp)
{
    fp[0].x = rand() % WIDTH;
    fp[0].y = rand() % HEIGHT;
    fp[0].color = 10;
    fp[0].duration = 30;
    fp[0].tick = 0;
}

void showFood(SnakeNode *fp)
{
    putASCII2((fp[0].x)*2  + OFFSET_X,
               fp[0].y + OFFSET_Y, 0xa1, fp[0].color, 0);
    putASCII2((fp[0].x)*2+1  + OFFSET_X,
               fp[0].y +  OFFSET_Y, 0xc0, fp[0].color, 0);
}
void moveFood(SnakeNode *fp)
{
    if (fp[0].tick < fp[0].duration) {
        fp[0].tick++;
    } else {
        fp[0].x = fp[0].x + rand()%3-1;
        fp[0].y = fp[0].y + rand()%3-1;

        if (fp[0].x > WIDTH-1) fp[0].x = 0;
        if (fp[0].x < 0) fp[0].x = WIDTH-1;
        if (fp[0].y > HEIGHT-1) fp[0].y = 0;
        if (fp[0].y < 0) fp[0].y = HEIGHT-1;
        fp[0].tick = 0;
    }
}


void genSnake(SnakeNode *sp, int length)
{
    int k;
    /* 0: right, 1: down, 2: left, 3: up */
    for (k=0; k<length; k++) {
        sp[k].x = 10 + k;
        sp[k].y = 10;
        sp[k].color = 15;
        sp[k].direction = 2;
        sp[k].duration = 10;
        sp[k].tick = 0;
    }
    sp[0].color = 14;
}

int addSnake(SnakeNode *sp, int length)
{
    int dx[] = {1, 0, -1, 0};
    int dy[] = {0, 1, 0, -1};
    /* 0: right, 1: down, 2: left, 3: up */
    if (length<MAX_LENGTH) {
        length++;
        sp[length-1].x = sp[length-2].x - dx[sp[length-2].direction];
        sp[length-1].y = sp[length-2].y - dy[sp[length-2].direction];
        sp[length-1].direction = sp[0].direction;
        sp[length-1].duration = sp[0].duration;
        sp[length-1].tick = 0;
        sp[length-1].color = 15;

    }
    return length;
}


void showSnake(SnakeNode *sp, int num)
{
    int k;

    for (k=0; k<num; k++) {
        putASCII2((sp[k].x)*2  + OFFSET_X,
               sp[k].y + OFFSET_Y, 0xA1, sp[k].color, 0);
        putASCII2((sp[k].x)*2+1  + OFFSET_X,
               sp[k].y +  OFFSET_Y, 0xDB, sp[k].color, 0);
    }
}

void moveSnake(SnakeNode *sp, int length)
{
    /* 0: right, 1: down, 2: left, 3: up */
    int dx[] = {1, 0, -1, 0};
    int dy[] = {0, 1, 0, -1};
    int k;

    if (sp[0].tick < sp[0].duration) {
        sp[0].tick++;
    } else {
        for (k=length-1; k>0; k--) {
            sp[k].x = sp[k-1].x;
            sp[k].y = sp[k-1].y;
            sp[k].direction = sp[k-1].direction;
        }
        sp[0].x = sp[0].x + dx[sp[0].direction];
        sp[0].y = sp[0].y + dy[sp[0].direction];

        for (k=0; k<length; k++) {
            if (sp[k].x > WIDTH-1) sp[k].x = 0;
            if (sp[k].x < 0) sp[k].x = WIDTH-1;
            if (sp[k].y > HEIGHT-1) sp[k].y = 0;
            if (sp[k].y < 0) sp[k].y = HEIGHT-1;
        }
        sp[0].tick = 0;
    }

}

void shiftRight(SnakeNode *sp, int length)
{
    /* 0: right, 1: down, 2: left, 3: up */
    int dx[] = {1, 0, -1, 0};
    int dy[] = {0, 1, 0, -1};
    int k;


        for (k=0; k<length; k++) {
        	sp[k].x += dx[(sp[k].direction+1)%4];
        	sp[k].y += dy[(sp[k].direction+1)%4];
        }


        for (k=0; k<length; k++) {
            if (sp[k].x > WIDTH-1) sp[k].x = 0;
            if (sp[k].x < 0) sp[k].x = WIDTH-1;
            if (sp[k].y > HEIGHT-1) sp[k].y = 0;
            if (sp[k].y < 0) sp[k].y = HEIGHT-1;
        }

	
}
void shiftLeft(SnakeNode *sp, int length)
{
    /* 0: right, 1: down, 2: left, 3: up */
    int dx[] = {1, 0, -1, 0};
    int dy[] = {0, 1, 0, -1};
    int k;


        for (k=0; k<length; k++) {
        	sp[k].x += dx[(sp[k].direction+3)%4];
        	sp[k].y += dy[(sp[k].direction+3)%4];
        }


        for (k=0; k<length; k++) {
            if (sp[k].x > WIDTH-1) sp[k].x = 0;
            if (sp[k].x < 0) sp[k].x = WIDTH-1;
            if (sp[k].y > HEIGHT-1) sp[k].y = 0;
            if (sp[k].y < 0) sp[k].y = HEIGHT-1;
        }

}


