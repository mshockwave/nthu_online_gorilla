#include <stdio.h>
#include <stdlib.h> /* 要使用 srand 和 rand 來產生亂數 */
#include <time.h>   /* 要使用 time 來產生 srand 所需的 seed */
#include <string.h> 
#include <windows.h> 
#include <mmsystem.h>
#include "console_draw2.h"  /* 在 DOS command window 畫圖 */
#include "kb_input2.h"
#define WIDTH     15  
#define NUM_GHOSTS 5
#define OFFSET_X  10
#define OFFSET_Y  5

void delay(float sec);

int main(void) 
{   
   int i, j;    
   int vk;
   int px = 0, py = 0; /* Jack 的位置 */
   char jack[] = {'M', 'W'};
   int state = 0; 
   int ctrl_down = 0; 
   int ghost_px[NUM_GHOSTS];
   int ghost_py[NUM_GHOSTS];
   int ghost_state[NUM_GHOSTS];
   float ghost_delta[NUM_GHOSTS]; 
   float ghost_timer[NUM_GHOSTS]; 
   int ghost_symbol[] = {'M', 'W'};
   int score = 0;
   char score_str[30]; 
   
   float SPF =  1.0/25.0; /* 設定每個畫面 停留 1.0/25.0 秒 */ 


   /* 設定 ghosts */ 
   for (i = 0; i < NUM_GHOSTS; i++) {
      ghost_px[i] = rand()%WIDTH;
      ghost_py[i] = rand()%WIDTH;
      ghost_state[i] = rand()%2;
      ghost_delta[i] = 0.1 + (float) rand()/RAND_MAX;
      ghost_timer[i] = 0;       
   } 
   
   initializeKeyInput(); /* 啟動鍵盤控制 */ 
	
   while (1) { /* 無窮迴圈 */
   
      /* 每個畫面要停留 SPF 秒  相當於每秒顯示 1.0/SPF 個畫面 */ 
      delay(SPF);
       
      
      /* 先處理鍵盤控制的部份 */           
      if(waitForKeyDown(0.001)) {
         vk=getKeyEventVirtual();	/* 讀取 virtual key */	  
                                     
         /* 判斷使用者按的按鍵對應到哪個移動方向  計算移動後的位置 */
         switch(vk) { 
            case VK_ESCAPE: 
               return 0; break;
            case VK_UP: 
               py--; break;
            case VK_DOWN: 
               py++; break;
            case VK_LEFT: 
               px--; break;
            case VK_RIGHT: 
               px++; break;
         }      
      }
      
      /* 
         if (KEY_DOWN(VK_SHIFT)) 
         這樣的方式判斷 某個鍵是否被按下 (例如 shift) 
         或是
         if (KEY_UP(VK_CONTROL))
         判斷 某個鍵是否已放開  (例如 control 鍵)          
      
         範例: 用 control 鍵 切換狀態   
         
         要完成一次完整的按鍵才會  改變狀態
         如果一直按著不放並不會發生作用
         (可以用這種方式去除"連發"的功能) 
      */
      if(KEY_DOWN(VK_CONTROL) && !ctrl_down) {
         ctrl_down = 1;
      }
      if(KEY_UP(VK_CONTROL) && ctrl_down) {
         state = (state+1) % 2;
         ctrl_down = 0;
      }
          
      /* 檢查 Jack 移動過後的位置是否超出範圍 */ 
      if (py < 0) py = 0; 
      if (py > WIDTH-1) py = WIDTH - 1;
      if (px < 0) px = 0; 
      if (px > WIDTH-1) px = WIDTH - 1;                


      /* 處理 ghosts 的移動 */ 
      for (i = 0; i < NUM_GHOSTS; i++) {
         ghost_timer[i] += SPF;
         if (ghost_timer[i] > ghost_delta[i]) { /* 時間到 可以走動了 */ 
            if (rand()%2) {  /* 亂走 */ 
               ghost_px[i]++; 
            } else {
               ghost_px[i]--;
            } 
            if (rand()%2) {
               ghost_py[i]++; 
            } else {
               ghost_py[i]--;
            } 
            ghost_timer[i] = 0; /* 重設 timer */          
         } 
         
         if (ghost_py[i] < 0) ghost_py[i] = 0; 
         if (ghost_py[i] > WIDTH-1) ghost_py[i] = WIDTH - 1;
         if (ghost_px[i] < 0) ghost_px[i] = 0; 
         if (ghost_px[i] > WIDTH-1) ghost_px[i] = WIDTH - 1;                         
                  
         if (ghost_px[i] == px && ghost_py[i] == py) {
            if (ghost_state[i] == state) { /* 符號一樣就可以吃掉 */ 
               score++;
               /* ghost 被吃掉  發出 XP 內建的音效 CHIMES */ 
               PlaySound("CHIMES", NULL, SND_ASYNC|SND_ALIAS);
               /* ghost 重生 */ 
               ghost_px[i] = rand()%WIDTH;
               ghost_py[i] = rand()%WIDTH;
               ghost_state[i] = rand()%2;
               ghost_delta[i] = 0.1 + (float) rand()/RAND_MAX;
               ghost_timer[i] = 0;       
            } 
         }     
      } 


      /* 底下開始處理顯示的部份 */ 
       
      for (i = 0; i < WIDTH; i++) {              
         for (j = 0; j < WIDTH; j++) {  
            putASCII2(i + OFFSET_X, j + OFFSET_Y, ' ', 0, 0);
         }
      }           
 
      /* 畫出 Jack 目前位置 */
      putASCII2(px + OFFSET_X, py + OFFSET_Y, jack[state], rand()%16, 4);

      /* 畫出 ghosts 目前位置 */
      for (i = 0; i < NUM_GHOSTS; i++) {              
         putASCII2(ghost_px[i] + OFFSET_X, ghost_py[i] + OFFSET_Y, 
            ghost_symbol[ghost_state[i]], 11, 0);         
      } 

      /* 顯示 score */
      sprintf(score_str, "score: %d", score);
      for (i = 0; i < strlen(score_str); i++) {
         putASCII2(10+i, 2, score_str[i], 15, 1);          
      } 
       

      /* 把填好的螢幕內容顯示出來 */
      drawCmdWindow();
   
   } /* while (1) 無窮迴圈 */

   return 0; 
} 

void delay(float sec)
{
   clock_t startc;  /* 計算停留的時間 */   
   startc = clock();
   for ( ; ; ) {	
	  if ((float)(clock()-startc)/CLOCKS_PER_SEC > sec) break;
   } 	 
} 
