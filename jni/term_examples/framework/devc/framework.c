#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <windows.h>
#include <mmsystem.h>
/* 音效記得要加入這個 */
#include "audio.h"
/* 另外還有底下的設定 */
/* For sound effect: In [Project Options]->[Parameters]->[Linker] add the parameter -lwinmm */


#include "console_draw2.h"
#include "kb_input2.h"


/*
putASCII2()  最後兩個參數分別是字元前景和背景的顏色
可以參考下面的顏色值對照表來設定你想要的顏色
   0: 黑     1: 暗藍   2: 暗綠   3: 暗青
   4: 暗紅   5: 暗紫   6: 暗黃   7: 淺灰
   8: 暗灰   9: 亮藍  10: 亮綠  11: 亮青
  12: 亮紅  13: 亮紫  14: 亮黃  15: 白
*/

#define WIDTH     30
#define HEIGHT    20
#define OFFSET_X  5
#define OFFSET_Y  5

#define NUM_KEYS 20
#define REFRESH_RATE 20

int plate[HEIGHT][WIDTH];


void putString(int x, int y, char *p, int fg_color, int bg_color);
int timer(float sec);

int my_game_one(void);
int my_game_two(void);

/*----------------------------------------------------------*/
struct t_menu {
    int x;
    int y;
    int fgcolor;
    int bgcolor;
    int cursor;
    int num_options;
    int state[10];
    char text[10][80]; /* 最多存十個字串  每個字串長度最多 79 個字元 */
    char alt_text[10][80]; /* 最多存十個字串  每個字串長度最多 79 個字元 */
};
typedef struct t_menu Menu;

void setMainmenu(Menu *m)
{
    int i;

    m->x = 10;
    m->y = 10;
    m->fgcolor = 13;
    m->bgcolor = 0;
    m->num_options = 5;
    m->cursor = 0;
    for (i=0; i<m->num_options; i++) {
        m->state[i] = 0;
    }
    m->state[m->cursor] = m->state[m->cursor] | 1;  /* 目前選擇的項目 */

    strcpy(m->text[0], "ONE PLAYER");
    strcpy(m->text[1], "TWO PLAYERS");
    strcpy(m->text[2], "START");
    strcpy(m->text[3], "EXIT");
    strcpy(m->text[4], "ABOUT");
    strcpy(m->alt_text[0], "ONE PLAYER *");
    strcpy(m->alt_text[1], "TWO PLAYERS *");
    strcpy(m->alt_text[2], "START *");
    strcpy(m->alt_text[3], "EXIT *");
    strcpy(m->alt_text[4], "ABOUT *");
}

int IsOnItem(Menu *m, int index)
{
    return (m->state[index] & 1); /* 用位元運算來判斷目前選擇的項目 */
}
int IsItemSelected(Menu *m, int index)
{
    return (m->state[index] & 2);
}
void showMenu(Menu *m)
{
    int i;
    int fg, bg;
    char *str;

    for (i=0; i<m->num_options; i++) {

        if ( !IsOnItem(m, i) ) {
            bg = m->bgcolor;
            fg = m->fgcolor;
        } else {
            fg = m->bgcolor;
            bg = m->fgcolor;
        }

        if ( !IsItemSelected(m, i) ) {
            str = m->text[i];
        } else {
            str = m->alt_text[i];
        }

        putString(m->x, m->y+i, str, fg, bg);
    }
}

void scrollMenu(Menu *m, int diff)
{
    m->state[m->cursor] = m->state[m->cursor] & (~1);  /* 把目前游標所在的選項狀態清除 */
    m->cursor = (m->cursor + diff + m->num_options) % m->num_options; /* 把遊標移到下一個選項 */
    m->state[m->cursor] = m->state[m->cursor] | 1; /* 選擇目前游標所在的選項 */
}

void radioMenu(Menu *m)
{
    int i;
    for (i=0; i<m->num_options; i++) {
        m->state[i] = m->state[i] & (~2); /* 清掉全部的選項 */
    }
    m->state[m->cursor] = m->state[m->cursor] | 2; /* 設定目前游標 */
}
void toggleMenu(Menu *m)
{
    m->state[m->cursor] = m->state[m->cursor] ^ 2;  /* 利用位元運算 產生 toggle 的效果 */
}


/*----------------------------------------------------------*/




int main(void)
{
    int IsEnding = 0;
    char logo[] = "MY BRILLIANT PC GAME FOR I2P12";

    int i, j, k;
    char str[40] = {'\0'};

    int key_down[NUM_KEYS] = {0};
    int key_val[NUM_KEYS] = {VK_UP, VK_DOWN, VK_ESCAPE, VK_RETURN, VK_SPACE};

    int cur_tick, last_tick;

    Menu mainmenu;

    /* 鋪上兩邊的牆壁和最底下的地板　*/
    for (i = 0; i < HEIGHT; i++) {
        plate[i][0] = 1;
        plate[i][WIDTH-1] = 1;
    }
    for (j = 0; j < WIDTH; j++) {
        plate[0][j] = 1;
        plate[HEIGHT-1][j] = 1;
    }


    /* 啟動鍵盤控制 整個程式中只要做一次就行了*/
    initializeKeyInput();

    setMainmenu(&mainmenu);

    /* 無窮迴圈  裡面包含了這個程式的主要工作
    而且每次迴圈會不斷把最新的畫面顯示到螢幕上
    像是播動畫一樣 每次迴圈更新一次畫面 */
    while (!IsEnding) {

        /* 把整個畫面清掉 並且重畫邊框 (不見得要用這樣的做法) */
        clearScreen();
        for (i = 0; i < HEIGHT; i++) {
            for (j = 0; j < WIDTH; j++) {
                if (plate[i][j] == 1) {
                    putASCII2(j*2 + OFFSET_X, i + OFFSET_Y, 0xA1, 8, 0);
                    putASCII2(j*2+1 + OFFSET_X, i + OFFSET_Y, 0xBD, 8, 0);
                } else {
                    putASCII2(j*2 + OFFSET_X, i + OFFSET_Y, ' ', 0, 0);
                    putASCII2(j*2+1 + OFFSET_X, i + OFFSET_Y, ' ', 0, 0);
                }
            }
        }
        /* 顯示 logo 以及遊戲資訊字串 */
        putString(OFFSET_X, OFFSET_Y-2, logo, 14, 3);
        sprintf(str, "%10d", timer(0.05));
        putString(OFFSET_X, OFFSET_Y-1, str, 14, 1);

        /* 把選單畫出來 */
        showMenu(&mainmenu);


        /* 每經過 REFRESH_RATE 個 ticks 才會更新一次畫面 */
        cur_tick = timer(0.005); /* 每個 tick 0.005 秒 */
        if (cur_tick % REFRESH_RATE == 0 && cur_tick != last_tick) {

            last_tick = cur_tick;

            /* 為了要讓一連串 putASCII2() 的動作產生效果
               必須要呼叫一次 drawCmdWindow() 把之前畫的全部內容一次顯示到螢幕上 */
            drawCmdWindow();

        } /* end of if (cur_tick % REFRESH_RATE == 0 ... */


        /* 鍵盤控制 處理按著不放的狀況 */
        for (k=0; k<NUM_KEYS; k++) {
            if(KEY_DOWN(key_val[k]) && !key_down[k]) {
                key_down[k] = 1;

                switch (key_val[k]) {
                case VK_UP:
                    scrollMenu(&mainmenu, -1);
                    break;
                case VK_DOWN:
                    scrollMenu(&mainmenu, +1);
                    break;
                case VK_RETURN:
                    if (IsOnItem(&mainmenu, 2)) {
                        if (IsItemSelected(&mainmenu, 0))
                            my_game_one();
                        if (IsItemSelected(&mainmenu, 1))
                            my_game_two();

                    } else if (IsOnItem(&mainmenu, 3)) {
                        IsEnding = 1;
                    }
                    break;
                case VK_SPACE:
                    toggleMenu(&mainmenu);
                    /* 可以試試看底下另一種選單形式
                        同時只能有一個項目可以被選到
                        radioMenu(&mainmenu);
                    */
                    break;
                }

            }

            /* 按鍵從原本被按下的狀態 變成放開的狀態  這是為了處理按著不放的情況 */
            if(KEY_UP(key_val[k]) && key_down[k]) {
                key_down[k] = 0;
            }

        }



    } /* while (IsEnding) */



    return 0;
}




/* 在 (x, y) 座標的位置顯示字串 p 的內容  fg_color 是前景的顏色  bg_color 則是背景的顏色 */
void putString(int x, int y, char *p, int fg_color, int bg_color)
{
    int i;
    for(i=0; i<strlen(p); i++) {
        putASCII2(x+i, y, p[i], fg_color, bg_color);
    }
}

int timer(float sec)
{
    static clock_t StartC; /* 利用 static 變數  累計 */
    static int Tick; /* 每經過 sec 秒 就讓 Tick 加一, 也就是說 每個 Tick 相當於 sec 秒 */

    if ((double)(clock()-StartC) > sec*CLOCKS_PER_SEC) { /* 用 if 條件判斷一次 檢查是否已達到預定的時間 */
        Tick++;
        StartC = clock();
    }

    return Tick;
}
/*--------------------------------------------------------------*/


int my_game_one(void)
{
    int IsEnding = 0;
    char logo[] = "MY BRILLIANT (OR FUNNY) GAME FOR I2P12";

    int i, j, k;
    char str[40] = {'\0'};

    int key_down[NUM_KEYS] = {0};
    int key_val[NUM_KEYS] = {VK_UP, VK_DOWN, VK_ESCAPE, VK_RETURN};

    int cur_tick, last_tick;


    /* 鋪上兩邊的牆壁和最底下的地板　*/
    for (i = 0; i < HEIGHT; i++) {
        plate[i][0] = 1;
        plate[i][WIDTH-1] = 1;
    }
    for (j = 0; j < WIDTH; j++) {
        plate[0][j] = 1;
        plate[HEIGHT-1][j] = 1;
    }


    /* 啟動鍵盤控制 整個程式中只要做一次就行了*/
    initializeKeyInput();

    while (!IsEnding) {


        /* 每經過 REFRESH_RATE 個 ticks 才會更新一次畫面 */
        cur_tick = timer(0.005);
        if (cur_tick % REFRESH_RATE == 0 && cur_tick != last_tick) {
            last_tick = cur_tick;
            clearScreen();
            for (i = 0; i < HEIGHT; i++) {
                for (j = 0; j < WIDTH; j++) {
                    if (plate[i][j] == 1) {
                        putASCII2(j*2 + OFFSET_X, i + OFFSET_Y, 0xA1, 8, 0);
                        putASCII2(j*2+1 + OFFSET_X, i + OFFSET_Y, 0xBD, 8, 0);
                    } else {
                        putASCII2(j*2 + OFFSET_X, i + OFFSET_Y, ' ', 0, 0);
                        putASCII2(j*2+1 + OFFSET_X, i + OFFSET_Y, ' ', 0, 0);
                    }
                }
            }

            i = rand()%15;
            j = rand()%15;
            putASCII2(j*2 + OFFSET_X, i + OFFSET_Y, 0xA1, 8, 0);
            putASCII2(j*2+1 + OFFSET_X, i + OFFSET_Y, 0xBD, 8, 0);

            putString(OFFSET_X, OFFSET_Y-2, logo, 14, 3);
            sprintf(str, "%10d", timer(0.05));
            putString(OFFSET_X, OFFSET_Y-1, str, 14, 1);

            drawCmdWindow();

        } /* end of if (cur_tick % REFRESH_RATE == 0 ... */

        for (k=0; k<NUM_KEYS; k++) {
            if(KEY_DOWN(key_val[k]) && !key_down[k]) {
                key_down[k] = 1;
                switch (key_val[k]) {
                case VK_ESCAPE:
                    IsEnding = 1;
                    break;
                }
            }
            if(KEY_UP(key_val[k]) && key_down[k]) {
                key_down[k] = 0;
            }

        }

    } /* while (IsEnding) */


    return 0;
}


int my_game_two(void)
{
    int IsEnding = 0;
    char logo[] = "MY BRILLIANT (OR FUNNY) GAME FOR I2P12";

    int i, j, k;
    char str[40] = {'\0'};

    int key_down[NUM_KEYS] = {0};
    int key_val[NUM_KEYS] = {VK_UP, VK_DOWN, VK_ESCAPE, VK_RETURN};

    int cur_tick, last_tick;


    /* 鋪上兩邊的牆壁和最底下的地板　*/
    for (i = 0; i < HEIGHT; i++) {
        plate[i][0] = 1;
        plate[i][WIDTH-1] = 1;
    }
    for (j = 0; j < WIDTH; j++) {
        plate[0][j] = 1;
        plate[HEIGHT-1][j] = 1;
    }


    /* 啟動鍵盤控制 整個程式中只要做一次就行了*/
    initializeKeyInput();

    while (!IsEnding) {



        /* 每經過 REFRESH_RATE 個 ticks 才會更新一次畫面 */
        cur_tick = timer(0.005);
        if (cur_tick % REFRESH_RATE == 0 && cur_tick != last_tick) {

            last_tick = cur_tick;


            clearScreen();

            for (i = 0; i < HEIGHT; i++) {
                for (j = 0; j < WIDTH; j++) {
                    if (plate[i][j] == 1) {
                        putASCII2(j*2 + OFFSET_X, i + OFFSET_Y, 0xA1, 8, 0);
                        putASCII2(j*2+1 + OFFSET_X, i + OFFSET_Y, 0xBD, 8, 0);
                    } else {
                        putASCII2(j*2 + OFFSET_X, i + OFFSET_Y, ' ', 0, 0);
                        putASCII2(j*2+1 + OFFSET_X, i + OFFSET_Y, ' ', 0, 0);
                    }
                }
            }

            i = rand()%15;
            j = rand()%15;
            putASCII2(j*2 + OFFSET_X, i + OFFSET_Y, 0xA1, 8, 0);
            putASCII2(j*2+1 + OFFSET_X, i + OFFSET_Y, 0xBD, 8, 0);

            i = rand()%15;
            j = rand()%15;
            putASCII2(j*2 + OFFSET_X, i + OFFSET_Y, 0xA1, 8, 0);
            putASCII2(j*2+1 + OFFSET_X, i + OFFSET_Y, 0xBD, 8, 0);

            putString(OFFSET_X, OFFSET_Y-2, logo, 14, 3);
            sprintf(str, "%10d", timer(0.05));
            putString(OFFSET_X, OFFSET_Y-1, str, 14, 1);

            drawCmdWindow();

            if (timer(0.05)>5000) IsEnding = 1;

        } /* end of if (cur_tick % REFRESH_RATE == 0 ... */

        for (k=0; k<NUM_KEYS; k++) {
            if(KEY_DOWN(key_val[k]) && !key_down[k]) {
                key_down[k] = 1;
                switch (key_val[k]) {
                case VK_ESCAPE:
                    IsEnding = 1;
                    break;
                }
            }
            if(KEY_UP(key_val[k]) && key_down[k]) {
                key_down[k] = 0;
            }

        }

    } /* while (IsEnding) */

    return 0;
}
