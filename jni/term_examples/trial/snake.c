#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <windows.h>
#include <mmsystem.h>
/* 音效記得要加入這個 */
#include "audio.h"
/* 另外還有底下的設定 */
/* For sound effect: In [Project Options]->[Parameters]->[Linker] add the parameter -lwinmm */


#include "console_draw2.h"
#include "kb_input2.h"

/*
putASCII2()  最後兩個參數分別是字元前景和背景的顏色
可以參考下面的顏色值對照表來設定你想要的顏色
   0: 黑     1: 暗藍   2: 暗綠   3: 暗青
   4: 暗紅   5: 暗紫   6: 暗黃   7: 淺灰
   8: 暗灰   9: 亮藍  10: 亮綠  11: 亮青
  12: 亮紅  13: 亮紫  14: 亮黃  15: 白
*/

#define WIDTH     30
#define HEIGHT    20
#define OFFSET_X  2
#define OFFSET_Y  2

#define MAX_LENGTH 50
#define REFRESH 10

struct t_SnakeNode {
   int x;
   int y;
   int color;
   int direction;
   int duration;
};
typedef struct t_SnakeNode SnakeNode;


void delay(float sec);
void putString(int x, int y, char *p, int fg_color, int bg_color);

void genSnake(SnakeNode *snake_p, int num);
void showSnake(SnakeNode *snake_p, int num);
void moveSnake(SnakeNode *snake_p, int num);


int ground[HEIGHT][WIDTH];

int main(void)
{
   int IsEnding = 0;
   char logo[] = "SAMPLE PROGRAM PROVIDED BY I2P11";
   int i, j;

   SnakeNode snake[MAX_LENGTH];
   int length = 5;

   clock_t startc;
   int tick = 0;
   int frame_update = 0;

   int vk;



   startc = clock();

   initializeKeyInput();

    for (i=0; i<WIDTH; i++) {
        ground[0][i] = 1;
        ground[HEIGHT-1][i] = 1;
    }
    for (i=0; i<HEIGHT; i++) {
        ground[i][0] = 1;
        ground[i][WIDTH-1] = 1;
    }


   genSnake(snake, length);

   while (!IsEnding) {

      if ((double)(clock()-startc) > 0.05*CLOCKS_PER_SEC) {
         tick++;
         frame_update = 0;
         startc = clock();
      }

      if(waitForKeyDown(0.001)) {
         vk=getKeyEventVirtual();

         switch(vk) {
            case VK_RIGHT:
                snake[0].direction = 0;
            break;
            case VK_DOWN:
                snake[0].direction = 1;
            break;
            case VK_LEFT:
                snake[0].direction = 2;
            break;
            case VK_UP:
                snake[0].direction = 3;
            break;
         }
      }
    if (tick > snake[0].duration) {

    clearScreen();


    for (i=0; i<HEIGHT; i++) {
        putASCII2(0  + OFFSET_X, i + OFFSET_Y, 0xA1, 10, 0);
         putASCII2(1  + OFFSET_X, i + OFFSET_Y, 0xBD, 10, 0);
        putASCII2((WIDTH-1)*2  + OFFSET_X, i + OFFSET_Y, 0xA1, 10, 0);
         putASCII2((WIDTH-1)*2+1  + OFFSET_X, i + OFFSET_Y, 0xBD, 10, 0);
    }
    for (j=0; j<WIDTH; j++) {
        putASCII2(j*2  + OFFSET_X, 0 + OFFSET_Y, 0xA1, 10, 0);
         putASCII2(j*2+1 + OFFSET_X, 0 + OFFSET_Y, 0xBD, 10, 0);
        putASCII2(j*2  + OFFSET_X, HEIGHT-1 + OFFSET_Y, 0xA1, 10, 0);
         putASCII2(j*2+1  + OFFSET_X, HEIGHT-1+ OFFSET_Y, 0xBD, 10, 0);
    }





        moveSnake(snake, length);
        tick = 0;


      showSnake(snake, length);

      putString(OFFSET_X, OFFSET_Y-2, logo, 14, 3);

      drawCmdWindow();
      }


   } /* while (IsEnding) */


   delay(0.5);
   return 0;
}




void delay(float sec)
{
   clock_t startc;
   startc = clock();
   for ( ; ; ) {
	  if ((float)(clock()-startc)/CLOCKS_PER_SEC > sec) break;
   }
}

void putString(int x, int y, char *p, int fg_color, int bg_color)
{
   int i;
   for(i=0; i<strlen(p); i++) {
      putASCII2(x+i, y, p[i], fg_color, bg_color);
   }
}


/****************************************************************************************************/

void moveSnake(SnakeNode *snake_p, int num)
{
   int k;
   int dx[] = {1, 0, -1, 0};
   int dy[] = {0, 1, 0, -1};

   for (k=num-1; k>=1; k--) {
      snake_p[k].x = snake_p[k-1].x;
      snake_p[k].y = snake_p[k-1].y;
      if (snake_p[k].x>WIDTH-2) snake_p[k].x = 1;
      if (snake_p[k].x<1) snake_p[k].x = WIDTH-2;
      if (snake_p[k].y>HEIGHT-2) snake_p[k].y = 1;
      if (snake_p[k].y<1) snake_p[k].y = HEIGHT-2;
   }
   snake_p[0].x += dx[snake_p[0].direction];
   snake_p[0].y += dy[snake_p[0].direction];

   for (k=num-1; k>=0; k--) {
      if (snake_p[k].x>WIDTH-2) snake_p[k].x = 1;
      if (snake_p[k].x<1) snake_p[k].x = WIDTH-2;
      if (snake_p[k].y>HEIGHT-2) snake_p[k].y = 1;
      if (snake_p[k].y<1) snake_p[k].y = HEIGHT-2;
   }
}

void genSnake(SnakeNode *snake_p, int num)
{
   int k;

   for (k=0; k<num; k++) {
      snake_p[k].x = 5 + k;
      snake_p[k].y = 5;
      snake_p[k].color = 10;

      snake_p[k].direction = 2;
      snake_p[k].duration = 1;
   }

}

void showSnake(SnakeNode *snake_p, int num)
{
   int k;

   for (k=0; k<num; k++) {
        putASCII2((snake_p[k].x)*2  + OFFSET_X,
                snake_p[k].y + OFFSET_Y, 0xA1, snake_p[k].color, 0);
        putASCII2((snake_p[k].x)*2+1  + OFFSET_X,
               snake_p[k].y +  OFFSET_Y, 0xBD, snake_p[k].color, 0);
   }

}


